# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create(email: 'f@f.com', password: '12345678', password_confirmation: '12345678', username: 'juanpintoduran')
UserProfile.create(name: 'Felipe', bio: '#Palta', color: '272727', location: 'SCL')

3.times do |i|
  Tweet.create(content: "This is the tweet number #{i+1}!", user: User.first)
end

Tweet.first.tweets = [Tweet.last]

List.create(name: 'Default', desc: 'Just another list', user: User.first)
ListMember.create(list: List.first, user: User.last)
